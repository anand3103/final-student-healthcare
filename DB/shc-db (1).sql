-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 25, 2019 at 11:40 PM
-- Server version: 5.6.43-cll-lve
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shc-db`
--

-- --------------------------------------------------------

--
-- Table structure for table `consultation`
--

CREATE TABLE `consultation` (
  `Id` int(11) NOT NULL,
  `Student_id` int(11) NOT NULL,
  `Doctor_id` int(11) NOT NULL,
  `Message` text NOT NULL,
  `Created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Updated_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `consultation`
--

INSERT INTO `consultation` (`Id`, `Student_id`, `Doctor_id`, `Message`, `Created_on`, `Updated_on`) VALUES
(1, 1, 25, 'test with amit', '2019-05-20 06:54:28', '0000-00-00 00:00:00'),
(2, 3, 25, 'test with pooja', '2019-05-20 06:54:39', '0000-00-00 00:00:00'),
(3, 1, 25, 'test with amit asdsd', '2019-05-20 06:54:46', '0000-00-00 00:00:00'),
(4, 4, 1, 'asdsadsadsa', '2019-06-03 06:05:19', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `doctor`
--

CREATE TABLE `doctor` (
  `Id` int(11) NOT NULL,
  `User_id` int(11) NOT NULL,
  `School` varchar(50) NOT NULL,
  `Specialist` varchar(50) NOT NULL,
  `Assocated_hospital` varchar(50) NOT NULL,
  `Designation` varchar(50) NOT NULL,
  `Address` text NOT NULL,
  `Img_url` varchar(255) NOT NULL,
  `Gender` varchar(50) NOT NULL,
  `Created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `doctor`
--

INSERT INTO `doctor` (`Id`, `User_id`, `School`, `Specialist`, `Assocated_hospital`, `Designation`, `Address`, `Img_url`, `Gender`, `Created_at`, `Updated_at`) VALUES
(1, 25, 'bits PILANI', 'sdasd', '', '', 'nanded', '', '', '2019-06-12 10:34:30', '2019-06-10 15:27:04'),
(2, 58, 'bits PILANI', 'sdsasdas', 'asdsadsad', 'zsssdfsdgf', 'asdsadsad', 'sasadasdsad.png', 'sasdasad', '2019-06-12 10:34:30', '2019-06-07 19:42:40'),
(3, 59, 'bits PILANI', 'sdf', '', '', '', '', '', '2019-06-12 10:34:30', '2019-06-10 14:37:21'),
(4, 60, 'bits PILANI', 'Docotoc', '', '', '', '', '', '2019-06-12 10:34:30', '2019-06-10 15:08:02'),
(5, 63, 'bits PILANI', '', '', '', '', '', '', '2019-06-12 10:34:30', '2019-06-10 15:29:36'),
(6, 64, 'bits PILANI', 'sdasd', '', '', '', '', '', '2019-06-12 10:34:30', '2019-06-10 16:41:02'),
(7, 65, 'bits PILANI', 'sdasd', '', '', '', '', '', '2019-06-12 10:34:30', '2019-06-10 16:43:19'),
(8, 66, 'bits PILANI', 'sdasd', '', '', '', '', '', '2019-06-12 10:34:30', '2019-06-10 16:45:54'),
(9, 67, 'bits PILANI', 'sdasd', '', '', '', '', '', '2019-06-12 10:34:30', '2019-06-10 16:47:42'),
(10, 68, 'bits PILANI', 'efse', '', '', '', '', '', '2019-06-12 10:34:30', '2019-06-10 16:51:02'),
(11, 70, 'bits PILANI', 'efse', '', '', '', '', '', '2019-06-12 10:34:30', '2019-06-10 16:56:00'),
(12, 71, 'bits PILANI', 'efse', '', '', '', '', '', '2019-06-12 10:34:30', '2019-06-10 16:58:14'),
(13, 72, 'bits PILANI', 'efse', '', '', '', '', '', '2019-06-12 10:34:30', '2019-06-10 17:02:56'),
(14, 73, 'bits PILANI', 'efse', '', '', '', '', '', '2019-06-12 10:34:30', '2019-06-10 17:05:35'),
(15, 74, 'bits PILANI', 'efse', '', '', '', '', '', '2019-06-12 10:34:30', '2019-06-10 17:06:24'),
(16, 75, 'bits PILANI', 'efse', '', '', '', '', '', '2019-06-12 10:34:30', '2019-06-10 17:19:28'),
(17, 76, 'bits PILANI', 'efse', '', '', '', '', '', '2019-06-12 10:34:30', '2019-06-10 17:20:15'),
(18, 77, 'bits PILANI', 'sdasd', 'nandur', '12-123', 'sadfas', '', '', '2019-06-12 10:34:30', '2019-06-10 17:23:22'),
(19, 78, 'Anand,bits PILANI', 'sdasd', 'nandur', '5-FEB-1999', 'nanded', '', '', '2019-06-14 10:31:42', '2019-06-10 17:25:49'),
(20, 79, 'bits PILANI', 'efse', 'sfe', 'fsdff', 'sdferwew', 'dsdffsdfsd.png', 'male', '2019-06-12 10:34:30', '2019-06-10 17:28:45'),
(21, 84, 'Array', 'kasdfj', 'surguru', 'asdkf', 'pune', '', '', '2019-06-12 20:20:36', '2019-06-12 20:20:36'),
(22, 85, 'Array', 'asd', 'sadfasdf', 'asd', 'sadasd', '', '', '2019-06-12 20:39:57', '2019-06-12 20:39:57'),
(23, 86, 'Array', 'qwe', 'nandur', '5-FEB-1999', 'qweqweqwe', '', '', '2019-06-12 20:50:34', '2019-06-12 20:50:34'),
(24, 87, '3,7', 'doctor', 'surya', 'dictire', 'pune', '', '', '2019-06-13 08:03:39', '2019-06-13 12:33:36'),
(25, 88, 'asdf, suryaa raj', 'sdasd', 'asdfasdf', '5-FEB-1999', 'Nanded', '', '', '2019-06-13 12:35:01', '2019-06-13 12:35:01'),
(26, 89, 'Anand,bits PILANI', 'sdasd', 'asdfasdf', '5-FEB-1999', 'pune', '', '', '2019-06-14 10:49:56', '2019-06-14 17:32:49');

-- --------------------------------------------------------

--
-- Table structure for table `eye_check`
--

CREATE TABLE `eye_check` (
  `Id` int(11) NOT NULL,
  `Student_id` int(11) NOT NULL,
  `Spherical_left` int(11) NOT NULL,
  `Spherical_right` int(11) NOT NULL,
  `Cylindrical_left` int(11) NOT NULL,
  `Cylindrical_right` int(11) NOT NULL,
  `Axis_left` int(11) NOT NULL,
  `Axis_right` int(11) NOT NULL,
  `Ap_left` int(11) NOT NULL,
  `Ap_right` int(11) NOT NULL,
  `Created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `eye_check`
--

INSERT INTO `eye_check` (`Id`, `Student_id`, `Spherical_left`, `Spherical_right`, `Cylindrical_left`, `Cylindrical_right`, `Axis_left`, `Axis_right`, `Ap_left`, `Ap_right`, `Created_at`, `Updated_at`) VALUES
(1, 8, 1, 2, 2, 3, 105, 2, 2, 2, '2019-06-03 06:06:25', '0000-00-00 00:00:00'),
(2, 14, 0, 0, 0, 0, 0, 0, 0, 0, '2019-06-14 13:20:39', '2019-06-14 13:20:39');

-- --------------------------------------------------------

--
-- Table structure for table `master_column`
--

CREATE TABLE `master_column` (
  `Id` int(11) NOT NULL,
  `Column_name` varchar(100) NOT NULL,
  `Img_url` varchar(255) NOT NULL,
  `Status` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_column`
--

INSERT INTO `master_column` (`Id`, `Column_name`, `Img_url`, `Status`) VALUES
(1, 'Height', 'height.png', '1'),
(2, 'Weight', 'weight.png', '1'),
(3, 'Bmi', 'BM.png', '0'),
(4, 'Bp', 'bp.png', '1'),
(5, 'Heart_rate', 'hr.png', '0'),
(6, 'Muscle_mass', 'mm.png', '0'),
(7, 'Body_fat', 'bf.png', '0'),
(8, 'Water', 'water.png', '1'),
(9, 'Basal_etabolism', 'basal.png', '0'),
(10, 'Visceral_fat', 'h.png', '0'),
(11, 'Bone_mass', 'bone.png', '1'),
(12, 'Dental_report', 'teeth.png', '1'),
(13, 'Eye_check', 'eye.png', '1');

-- --------------------------------------------------------

--
-- Table structure for table `parent_post_data`
--

CREATE TABLE `parent_post_data` (
  `Id` int(11) NOT NULL,
  `Student_id` int(11) NOT NULL,
  `Allergies` varchar(255) DEFAULT NULL,
  `Chronic_illness` varchar(255) DEFAULT NULL,
  `Medication` varchar(255) DEFAULT NULL,
  `Genetic` varchar(255) DEFAULT NULL,
  `Created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `parent_post_data`
--

INSERT INTO `parent_post_data` (`Id`, `Student_id`, `Allergies`, `Chronic_illness`, `Medication`, `Genetic`, `Created_at`, `Updated_at`) VALUES
(1, 3, 'sadsa', NULL, NULL, NULL, '2019-05-15 17:43:49', '2019-05-15 17:43:49'),
(2, 3, 'sadsa', 'sadsad', 'sadsad', 'sadsa', '2019-05-15 17:57:58', '2019-05-15 17:57:58'),
(3, 1, 'sadsa', 'sadsad', 'sadsad', 'sadsa', '2019-05-15 17:58:10', '2019-05-15 17:58:10'),
(4, 1, 'sadsa', 'sadsad', 'sadsad', 'sadsa', '2019-05-15 18:02:12', '2019-05-15 18:02:12'),
(5, 1, 'sadsa', 'sadsad', 'sadsad', 'sadsa', '2019-05-15 18:02:22', '2019-05-15 18:02:22'),
(6, 1, 'dsfdfs', 'dfghgfgfg', 'dfgdfgdfg', 'gdfgdfgdfg', '2019-05-15 18:14:00', '2019-05-15 18:14:00'),
(7, 1, 'dsfdfs', 'dfghgfgfg', 'dfgdfgdfg', 'gdfgdfgdfg', '2019-05-15 18:15:18', '2019-05-15 18:15:18'),
(8, 1, 'dkjhfdsafkjha', 'dsfkljdsfdslkfjdslfjsdfjsf', 'dslkjfdslfdsfjsdfjdsfkl', 'dsfjhkdsfkjsdfhjsdfkj', '2019-05-15 19:16:03', '2019-05-15 19:16:03'),
(9, 1, 'hskdhsakdhjsa', 'clkjasdaksflaskjdslkj', 'sakjdsa;dlksakjdl', 'sldfljdsf;lkjdsfkjdsflk', '2019-05-15 19:18:12', '2019-05-15 19:18:12'),
(10, 1, 'Anand baba ki jai', 'Sdkjfgsahkjfdsahfj', 'Ddsfasafkjdfs', 'Dvdslkfsadfdsf', '2019-05-15 19:22:08', '2019-05-15 19:22:08'),
(11, 1, 'fuijdsfjdsjfdslkwf', 'bdsfjfdsfkn', 'dsoidsfsujfdsj', 'dsdsghdsgkjn', '2019-05-16 13:22:00', '2019-05-16 13:22:00'),
(12, 1, 'fdsfddsfsdf', 'fcgdfgdfg', 'gdfgfddg', 'fdgfdgdfg', '2019-05-16 19:30:32', '2019-05-16 19:30:32'),
(13, 1, 'fdsfddsfsdf', 'fcgdfgdfg', 'gdfgfddg', 'fdgfdgdfg', '2019-05-16 19:30:32', '2019-05-16 19:30:32'),
(14, 1, 'fdsfddsfsdf', 'fcgdfgdfg', 'gdfgfddg', 'fdgfdgdfg', '2019-05-16 19:30:32', '2019-05-16 19:30:32'),
(15, 1, 'fdsfddsfsdf', 'fcgdfgdfg', 'gdfgfddg', 'fdgfdgdfg', '2019-05-16 19:30:33', '2019-05-16 19:30:33'),
(16, 1, 'fdsfddsfsdf', 'fcgdfgdfg', 'gdfgfddg', 'fdgfdgdfg', '2019-05-16 19:30:33', '2019-05-16 19:30:33'),
(17, 1, 'fdsfddsfsdf', 'fcgdfgdfg', 'gdfgfddg', '', '2019-05-16 19:30:46', '2019-05-16 19:30:46'),
(18, 1, 'fdsfddsfsdf', 'fcgdfgdfg', 'gdfgfddg', '', '2019-05-16 19:56:31', '2019-05-16 19:56:31'),
(19, 1, 'fdsfddsfsdf', '', 'gdfgfddg', '', '2019-05-16 19:56:42', '2019-05-16 19:56:42'),
(20, 1, 'fssdf', 'asxasdas', 'sadad', 'sdsasadad', '2019-05-16 19:57:25', '2019-05-16 19:57:25'),
(21, 1, 'dsfdfs', 'dfghgfgfg', 'dfgdfgdfg', 'gdfgdfgdfg', '2019-05-20 13:27:11', '2019-05-20 13:27:11'),
(22, 1, 'g', 'g', 'h', 'g', '2019-06-03 14:43:12', '2019-06-03 14:43:12'),
(23, 1, 'g', 'g', 'h', 'g', '2019-06-03 14:43:12', '2019-06-03 14:43:12'),
(24, 1, 'g', 'g', 'h', 'g', '2019-06-03 14:43:12', '2019-06-03 14:43:12'),
(25, 1, 'g', 'g', 'h', 'g', '2019-06-03 14:43:12', '2019-06-03 14:43:12'),
(26, 1, 'g', 'g', 'h', 'g', '2019-06-03 14:43:12', '2019-06-03 14:43:12'),
(27, 1, 'g', 'g', 'h', 'g', '2019-06-03 14:43:12', '2019-06-03 14:43:12'),
(28, 1, 'g', 'g', 'h', 'g', '2019-06-03 14:43:12', '2019-06-03 14:43:12'),
(29, 1, 'g', 'g', 'h', 'g', '2019-06-03 14:43:13', '2019-06-03 14:43:13'),
(30, 1, 'g', 'g', 'h', 'g', '2019-06-03 14:43:20', '2019-06-03 14:43:20'),
(31, 1, 'g', 'g', 'h', 'g', '2019-06-03 14:43:23', '2019-06-03 14:43:23'),
(32, 1, 'g', 'g', 'h', 'g', '2019-06-03 14:43:25', '2019-06-03 14:43:25'),
(33, 1, 'g', 'g', 'h', 'g', '2019-06-03 14:43:25', '2019-06-03 14:43:25'),
(34, 1, 'g', 'g', 'h', 'g', '2019-06-03 14:43:25', '2019-06-03 14:43:25'),
(35, 1, 'g', 'g', 'h', 'g', '2019-06-03 14:43:25', '2019-06-03 14:43:25'),
(36, 1, 'g', 'g', 'h', 'g', '2019-06-03 14:43:25', '2019-06-03 14:43:25'),
(37, 1, 'g', 'g', 'h', 'g', '2019-06-03 14:43:25', '2019-06-03 14:43:25'),
(38, 1, 'vv', 'g', 'g', 'ug', '2019-06-05 11:49:11', '2019-06-05 11:49:11'),
(39, 1, 'vv', 'g', 'g', 'ug', '2019-06-05 11:49:11', '2019-06-05 11:49:11'),
(40, 1, 'vv', 'g', 'g', 'ug', '2019-06-05 11:49:11', '2019-06-05 11:49:11'),
(41, 1, 'vv', 'g', 'g', 'ug', '2019-06-05 11:49:11', '2019-06-05 11:49:11'),
(42, 1, 'vv', 'g', 'g', 'ug', '2019-06-05 11:49:11', '2019-06-05 11:49:11'),
(43, 1, 'vv', 'g', 'g', 'ug', '2019-06-05 11:49:11', '2019-06-05 11:49:11'),
(44, 1, 'vv', 'g', 'g', 'ug', '2019-06-05 11:49:11', '2019-06-05 11:49:11'),
(45, 1, 'vv', 'g', 'g', 'ug', '2019-06-05 11:49:12', '2019-06-05 11:49:12'),
(46, 1, 'vv', 'g', 'g', 'ug', '2019-06-05 11:49:12', '2019-06-05 11:49:12'),
(47, 1, 'vv', 'g', 'g', 'ug', '2019-06-05 11:49:12', '2019-06-05 11:49:12'),
(48, 1, 'dsafsdfdsf', 'dsfsffs', 'dsfsfs', 'fdsfsdfsf', '2019-06-05 17:00:17', '2019-06-05 17:00:17'),
(49, 1, 'dsafsdfdsf', 'dsfsffs', 'dsfsfs', 'fdsfsdfsf', '2019-06-05 17:01:11', '2019-06-05 17:01:11'),
(50, 1, 'sgh', 'sbns', 'sbb', 'sbshj', '2019-06-05 18:02:14', '2019-06-05 18:02:14'),
(51, 1, 'cyctc', 'vhvt', 'xvh', 'rffs', '2019-06-05 18:11:30', '2019-06-05 18:11:30'),
(52, 1, 'cyctc', 'vhvt', 'xvh', '', '2019-06-05 18:11:39', '2019-06-05 18:11:39'),
(53, 1, 'cyctc', 'vhvt', '', '', '2019-06-05 18:11:51', '2019-06-05 18:11:51'),
(54, 1, 'tfvv', 'yff', 'gfvh', 'vffgb', '2019-06-05 18:12:29', '2019-06-05 18:12:29'),
(55, 1, 'dfssfdf', ' ', ' ', ' ', '2019-06-05 19:07:48', '2019-06-05 19:07:48'),
(56, 1, 'ghh', ' ', ' ', ' ', '2019-06-05 19:33:17', '2019-06-05 19:33:17'),
(57, 1, 'salarmsmmdn', ' dhsjsj', ' dhdjdj', ' dhdjks', '2019-06-05 19:38:47', '2019-06-05 19:38:47'),
(58, 1, 'ggg', ' ', ' ', ' ', '2019-06-05 23:33:37', '2019-06-05 23:33:37'),
(59, 1, 'Dhh', ' ', ' ', ' ', '2019-06-06 13:02:23', '2019-06-06 13:02:23'),
(60, 1, 'Dhh', ' ', ' ', ' ', '2019-06-06 13:02:38', '2019-06-06 13:02:38'),
(61, 1, 'Gg', ' ', ' ', ' ', '2019-06-06 14:33:11', '2019-06-06 14:33:11'),
(62, 3, 'Gg', ' ', ' ', ' ', '2019-06-06 14:33:33', '2019-06-06 14:33:33');

-- --------------------------------------------------------

--
-- Table structure for table `school`
--

CREATE TABLE `school` (
  `Id` int(11) NOT NULL,
  `User_id` int(11) NOT NULL,
  `School_name` varchar(100) NOT NULL,
  `Admin_name` varchar(50) NOT NULL,
  `Website_url` varchar(50) DEFAULT NULL,
  `Class_from` varchar(50) NOT NULL,
  `Class_to` varchar(50) NOT NULL,
  `Address` text NOT NULL,
  `Logo_url` varchar(255) NOT NULL,
  `Created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `school`
--

INSERT INTO `school` (`Id`, `User_id`, `School_name`, `Admin_name`, `Website_url`, `Class_from`, `Class_to`, `Address`, `Logo_url`, `Created_at`, `Updated_at`) VALUES
(3, 14, 'Anand', 'sadfasdf', 'ssdfsdfsfssd', 'Std 4', 'Std 9', 'Nanded', '', '2019-06-10 08:28:20', '2019-06-10 15:28:20'),
(4, 39, '', '', NULL, '', '', '', '', '2019-06-06 12:25:27', '2019-06-06 19:25:27'),
(5, 40, 'alskjdfkl', 'a;lsdjl;fjka0', NULL, '', '', '', '', '2019-06-05 20:14:16', '2019-06-05 20:14:16'),
(6, 41, 'dsf', '', NULL, '', '', '', '', '2019-06-05 20:19:01', '2019-06-05 20:19:01'),
(7, 42, 'bits PILANI', '', NULL, '', '', '', '', '2019-06-06 12:24:50', '2019-06-06 19:24:50'),
(8, 43, 'asdf', 'sadf', NULL, '', '', '', '', '2019-06-06 18:59:49', '2019-06-06 18:59:49'),
(9, 44, 'aaaaaaa', 'sdfasdfasdf', NULL, '', '', '', '', '2019-06-06 19:01:01', '2019-06-06 19:01:01'),
(10, 45, 'suryaa raj', 'ksajkldf', 'asd', 'Std 9', 'Std 9', 'asd', '', '2019-06-06 12:48:50', '2019-06-06 19:48:50'),
(11, 46, 'Raj kumar Patil', 'aksjlf', NULL, '', '', '', '', '2019-06-06 12:52:26', '2019-06-06 19:52:26'),
(12, 47, 'Pratik Bhau', 'kasjdfljl', 'http://www.jalsjdklf.com', 'Std 1', 'Std 10', 'Pune', '', '2019-06-06 13:39:50', '2019-06-06 20:39:50');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `Id` int(11) NOT NULL,
  `User_id` int(11) NOT NULL,
  `Student_first` varchar(50) NOT NULL,
  `Student_last` varchar(50) NOT NULL,
  `School` varchar(50) NOT NULL,
  `Class` varchar(50) NOT NULL,
  `Division` varchar(50) NOT NULL,
  `Mother_name` varchar(50) NOT NULL,
  `Mother_mobile` varchar(50) NOT NULL,
  `Father_mobile` varchar(50) NOT NULL,
  `Mother_email` varchar(100) NOT NULL,
  `Date_of_birth` varchar(50) NOT NULL,
  `School_logo` varchar(50) NOT NULL,
  `Student_img` varchar(255) NOT NULL DEFAULT 'icons8-children-filled-50.png',
  `blood_group` varchar(50) NOT NULL,
  `Address` text NOT NULL,
  `Gender` varchar(50) NOT NULL,
  `Created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`Id`, `User_id`, `Student_first`, `Student_last`, `School`, `Class`, `Division`, `Mother_name`, `Mother_mobile`, `Father_mobile`, `Mother_email`, `Date_of_birth`, `School_logo`, `Student_img`, `blood_group`, `Address`, `Gender`, `Created_at`, `Updated_at`) VALUES
(1, 37, 'Amit', 'Kadam', 'XYZ International School', '7th', 'B', 'Sonal', '1234569870', '147852369', 'sonal@gmail.com', '5-FEB-1999', 'sdsadsad56757dsa.png', 'sadfsdh1465465.png', 'A+', 'sadsadsad', 'male', '2019-06-17 07:15:18', '2019-06-10 20:52:52'),
(3, 37, 'pooja', 'kadam', 'd.y.patil', '5th', 'C', 'Sonal', '1234562870', '147852369', 'sonasl@gmail.com', '5-FEB-1996', 'sdsadsad56757dsa.png', 'weyirweury41545.jpg', 'AB-', 'sadsadsad', 'Female', '2019-05-15 09:59:21', '0000-00-00 00:00:00'),
(4, 38, 'sonal', 'deshmikh', 'd.y.patil', '7th', 'B', 'Pooja', '1478523690', '3214569870', 'pooja@gmail.com', '5-FEB-1996', '', 'icons8-children-filled-50.png', 'B+', 'sadsadsa', 'Male', '2019-05-20 10:27:31', '0000-00-00 00:00:00'),
(8, 38, 'kunal', 'deshmikh', 'sadsad', '7th', 'B', 'Pooja', '14785655', '3214569870', 'poojas@gmail.com', '5-FEB-1996', '', 'icons8-children-filled-50.png', 'B+', 'sadsadsa', 'Male', '2019-05-20 10:27:31', '0000-00-00 00:00:00'),
(9, 51, '', '', 'dasdsad', 'sadsad', 'sadsad', 'sadsad', '21323213', '', 'sadshhad@sadhsad.com', '213213', 'sadsad', 'icons8-children-filled-50.png', '', 'sadsad', 'sadsad', '2019-06-06 21:52:52', '2019-06-06 21:52:52'),
(10, 56, '', '', 'asjkldf', 'kasjdkf', 'A', 'jlkasouiew', '797979897898', '', 'Akljasdjfk@fma.com', '44555', 'sadfa', 'icons8-children-filled-50.png', '', 'lkasdjfkl', 'ajsdfjkl', '2019-06-06 22:04:57', '2019-06-06 22:04:57'),
(11, 57, '', '', 'PQR International School', 'Std 8', 'A', 'asdfasd', '234234234', '', 'suryakasdfjkl@gmail.com', '12-12323', '', 'icons8-children-filled-50.png', '', '234234', '', '2019-06-11 08:32:08', '2019-06-11 15:32:08'),
(12, 80, 'kumer', 'mumer', 'MNO International School', 'Std 3', 'C', 'kskdfjk', '324234234', '', 'klsjdkfl@gmilc.com', 'sf', '', 'icons8-children-filled-50.png', '', 'pune', '', '2019-06-11 14:54:59', '2019-06-11 14:54:59'),
(13, 81, 'kumer', 'kljsdkf', 'ABC International School', 'Std 2', 'C', 'kemer', '9797977998', '', 'kemer@gmil.com', 'lksdj', '', 'icons8-children-filled-50.png', '', 'punee', '', '2019-06-11 15:02:04', '2019-06-11 15:02:04'),
(14, 82, 'sumit', 'patil', 'Anand', 'Std 3', 'C', 'Chandra', '798798798798', '', 'cdhan@gm,ailc.om', 'slksda', '', 'icons8-children-filled-50.png', '', 'p[une', '', '2019-06-14 05:46:55', '2019-06-12 14:28:37'),
(15, 83, 'sneha', 'patil', 'bits PILANI', 'Std 3', 'B', 'Chandra', '789798798798', '', 'ssdfsuryakasdfjkl@gmail.com', '5-FEB-1999', '', 'icons8-children-filled-50.png', '', 'nanded', '', '2019-06-14 09:31:49', '2019-06-12 14:33:21');

-- --------------------------------------------------------

--
-- Table structure for table `student_health`
--

CREATE TABLE `student_health` (
  `Id` int(11) NOT NULL,
  `Student_id` int(11) NOT NULL,
  `Height` varchar(50) DEFAULT NULL,
  `Weight` varchar(50) DEFAULT NULL,
  `Bmi` varchar(50) DEFAULT NULL,
  `Bp` varchar(50) DEFAULT NULL,
  `Heart_rate` varchar(50) DEFAULT NULL,
  `Muscle_mass` varchar(50) DEFAULT NULL,
  `Body_fat` varchar(50) DEFAULT NULL,
  `Water` varchar(50) DEFAULT NULL,
  `Basal_etabolism` varchar(50) DEFAULT NULL,
  `Visceral_fat` varchar(50) DEFAULT NULL,
  `Bone_mass` varchar(50) DEFAULT NULL,
  `Dental_report` varchar(255) NOT NULL,
  `Disease` text,
  `Created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_health`
--

INSERT INTO `student_health` (`Id`, `Student_id`, `Height`, `Weight`, `Bmi`, `Bp`, `Heart_rate`, `Muscle_mass`, `Body_fat`, `Water`, `Basal_etabolism`, `Visceral_fat`, `Bone_mass`, `Dental_report`, `Disease`, `Created_at`, `Updated_at`) VALUES
(1, 1, '3.9', '50', '78', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, '2019-06-03 08:16:36', '0000-00-00 00:00:00'),
(2, 3, '4.2', '55', '123', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, '2019-06-03 08:16:24', '0000-00-00 00:00:00'),
(3, 1, '8.9', '53', '122', '2123', '321', '21', '32321', '213', '321', '3213', 'ssa', '0', NULL, '2019-06-11 09:26:56', '0000-00-00 00:00:00'),
(4, 1, '7.5', '43', '12', '21', '321', '21', '32', '213', '32', '32', 'ssa', '0', NULL, '2019-06-11 09:27:25', '0000-00-00 00:00:00'),
(5, 1, '4.6', '55', '87', '87', '122', NULL, '66', NULL, NULL, NULL, NULL, '0', NULL, '2019-06-03 08:16:36', '0000-00-00 00:00:00'),
(6, 1, '6.3', '57', '122', '212', '78', '21', '32', '213', '321', '3213', 'ssa', '0', NULL, '2019-06-11 09:28:01', '0000-00-00 00:00:00'),
(7, 14, 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'asd', 'asdf', NULL, 'asdf', '', 'adsf', '2019-06-14 13:15:30', '2019-06-14 13:15:30'),
(9, 15, 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'asd', 'asdf', NULL, 'asdf', '', 'adsf', '2019-06-14 07:15:28', '2019-06-14 13:20:39');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `Id` int(11) NOT NULL,
  `First_name` varchar(50) NOT NULL,
  `Last_name` varchar(50) DEFAULT NULL,
  `Email` varchar(100) NOT NULL,
  `Mobile` varchar(50) NOT NULL,
  `User_type` enum('1','2','3','4') NOT NULL DEFAULT '1' COMMENT '"1"=>"super admin", "2"=>"doctor", "3"=>"school", "4"=>"student"',
  `Password` varchar(255) NOT NULL,
  `otp` int(11) DEFAULT NULL,
  `Status` enum('0','1') NOT NULL,
  `Jwt_token` text NOT NULL,
  `Created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`Id`, `First_name`, `Last_name`, `Email`, `Mobile`, `User_type`, `Password`, `otp`, `Status`, `Jwt_token`, `Created_at`, `Updated_at`) VALUES
(3, '', '', 'admin@admin.com', '1234568790', '1', 'e10adc3949ba59abbe56e057f20f883e', NULL, '1', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjp7ImVtYWlsIjoiYWRtaW4uYWRtaW4uY29tIiwicGFzc3dvcmQiOiIxMjM0NTYifX0.Wi8ansd3V0JVAD3Ul89cfF2RuuuodtCaSZ5sh1JWvw4', '2019-04-10 09:13:06', '2019-04-10 05:17:30'),
(14, '', '', 'asd@sdsad.com', '1234569870', '3', '', NULL, '0', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjoiYXNkQHNkc2FkLmNvbSJ9.DOmP-u4N_z-hlcAQaW15Tby_aHMTB7J_ZnDjelJ2oj8', '2019-04-10 08:45:10', '2019-04-10 08:45:10'),
(25, '', 'patil', 'eeeee@asd.com', '147852326565', '2', '', NULL, '0', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjoiZWVlZWVAYXNkLmNvbSJ9.i5tAKCtOllYpZwB75Ku2AIwBobAlv7aU-0-7h1MuJXc', '2019-05-20 07:15:54', '2019-04-10 09:45:59'),
(33, '', '', 'asdsad@asdsad.com', '12365487987', '2', '', 155550, '0', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjoiYXNkc2FkQGFzZHNhZC5jb20ifQ.mGKN8WTzDK3I2vSvGfjCimCZD-ygUVc3KdGvwICoZGg', '2019-06-05 09:42:42', '2019-06-05 16:42:42'),
(35, '', '', 'asdsad@asdsad.coms', '12365487987', '2', '', 155550, '0', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjoiYXNkc2FkQGFzZHNhZC5jb21zIn0.Vx5wQlFzV__tPvihW-WkcCtrbJ0vNhhUxKvJOSGBkyY', '2019-06-05 09:42:42', '2019-06-05 16:42:42'),
(36, '', '', 'anand.kulkarni@connexistech.com', '12365487987', '2', '', 155550, '0', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjoiYW5hbmQua3Vsa2FybmlAY29ubmV4aXN0ZWNoLmNvbSJ9.rPUT5jPcFL3JXnKMWdD7j9ZORF34YqQaH17FCuVteJo', '2019-06-05 09:42:42', '2019-06-05 16:42:42'),
(37, '', '', 'sumit@gmail.com', '8669149917', '4', '25d55ad283aa400af464c76d713c07ad', 756521, '1', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjoisssssXNkc2FkQGFzZHNhZC5jb20ifQ.mGKN8WTzDK3I2vSvGfjCimCZD-ygUVc3KdGvwICoZGg', '2019-06-06 14:44:49', '2019-06-06 21:44:49'),
(38, '', 'deshmukh', 'pooja@deshmukh.com', '1234567890', '4', 'e10adc3949ba59abbe56e057f20f883e', 662068, '1', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjoipppppXNkc2FkQGFzZHNhZC5jb20ifQ.mGKN8PPzDK3I2vSvGfjCimCZD-ygUVc3KdGvwICoZGg', '2019-06-05 08:13:05', '2019-06-05 15:13:05'),
(39, '', 'pokli', 'andy@test.com', '1478523690', '3', 'e10adc3949ba59abbe56e057f20f883e', NULL, '1', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjoiYW5keUB0ZXN0LmNvbSJ9.tN8YWUExRmhimmq8LyQgOnm98qmIq7wAi9qGInz_iII', '2019-06-12 07:45:29', '2019-06-05 19:47:01'),
(40, '', NULL, 'lkasjdfllaskdf', 'aslkdjfkla', '3', '', NULL, '0', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjoibGthc2pkZmxsYXNrZGYifQ.Rn5-QkdLiO5p7aYmDqLOf0Dv_1DEcNZXewzJZien-0o', '2019-06-05 20:14:16', '2019-06-05 20:14:16'),
(41, '', NULL, 'sdfg', 'dsfg', '3', '', NULL, '0', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjoic2RmZyJ9.skAUxX5FaFzC1Kbw89QaMIs2g1yO1ugfCt10c6PqyRg', '2019-06-05 20:19:01', '2019-06-05 20:19:01'),
(42, '', NULL, '', '', '3', '', NULL, '0', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjoiYXNkZkBhc2RmIn0.kF08s-XmJMINTeD3FCLtulKAOMK3_ESxLJbP1XpLMG8', '2019-06-06 12:24:50', '2019-06-06 19:24:50'),
(43, '', NULL, 'sumit@gmail.com', 'asdf', '3', '', NULL, '0', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjoic3VtaXRAZ21haWwuY29tIn0.VomhgXjICFn0mbR1PMk8fey2GYa8-9BdpyrOZ8ija38', '2019-06-06 18:59:49', '2019-06-06 18:59:49'),
(44, '', NULL, 'asdf@asdf', '23243342432', '3', '', NULL, '0', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjoiYXNkZkBhc2RmIn0.kF08s-XmJMINTeD3FCLtulKAOMK3_ESxLJbP1XpLMG8', '2019-06-06 19:01:01', '2019-06-06 19:01:01'),
(45, '', NULL, 'kasjdfkl', '779+879+798', '3', '', NULL, '0', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjoia2FzamRma2wifQ.TZfQpUbsfaCH_v6wDlbPuQMCo458keqTYRf7L99K9YM', '2019-06-06 19:47:29', '2019-06-06 19:47:29'),
(46, '', NULL, 'kajskdlf', '798798798798', '3', '', NULL, '0', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjoia2Fqc2tkbGYifQ.O3qeO-se1L1V6LJ66U5szI_xV-ybR190FroGkfmMpzQ', '2019-06-06 19:50:02', '2019-06-06 19:50:02'),
(47, '', NULL, 'kjklasdfjlkasdjfkl', '98798978798', '3', '', NULL, '0', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjoia2prbGFzZGZqbGthc2RqZmtsIn0.iSkbBlmRLeoZ5ovob-snjy0RYyqUP-XFQCFF6buBGRU', '2019-06-06 20:38:34', '2019-06-06 20:38:34'),
(48, '', NULL, 'sadsad@asdsa.com', '123213213', '4', '', NULL, '0', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjoic2Fkc2FkQGFzZHNhLmNvbSJ9.wLqOGmJ7w8MIaKVuQ-eTXp97kDHa-J4H1sKygGpHi2Q', '2019-06-06 21:51:40', '2019-06-06 21:51:40'),
(51, '', NULL, 'sadsjjad@asdsa.com', '123213213', '4', '', NULL, '0', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjoic2Fkc2pqYWRAYXNkc2EuY29tIn0.Z3DYmIJHltWosHDLsDyG4ukWa2DoZ_h7A8JzUXqNKEE', '2019-06-06 21:52:52', '2019-06-06 21:52:52'),
(53, '', NULL, 'lasjdfkl@gmailc.om', '09823789439', '4', '', NULL, '0', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjoibGFzamRma2xAZ21haWxjLm9tIn0.h6vmpYqFcPeBCahn3QrU0scGtl7XBW7UPlaoi2Ko8aE', '2019-06-06 22:03:16', '2019-06-06 22:03:16'),
(56, '', NULL, 'lasj12132dfkl@gmailc.om', '09823789439', '4', '', NULL, '0', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjoibGFzajEyMTMyZGZrbEBnbWFpbGMub20ifQ.-UsXATg0bcqFDKPQoVlDCPYZvtkhHCyw0Jz5AixqQDA', '2019-06-06 22:04:57', '2019-06-06 22:04:57'),
(57, '', NULL, '', '234234234234', '4', '', NULL, '0', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjoiIn0.AkBMVagpvBQwB9SHx1cym4Yead-opAPKKR6V6Ac63J0', '2019-06-06 22:07:37', '2019-06-06 22:07:37'),
(58, '', 'sadsad', 'docotr@fsdf.ocm', '12322213213', '2', '', NULL, '0', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjpudWxsfQ.9lvpmeQZHsbFGYA7uW685SVqcqFTiGUoD4jzqMT5Aok', '2019-06-07 12:42:40', '2019-06-07 19:42:40'),
(59, '', NULL, 'asfasfd@gmail.com', '12341231231', '2', '', NULL, '0', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjpudWxsfQ.9lvpmeQZHsbFGYA7uW685SVqcqFTiGUoD4jzqMT5Aok', '2019-06-10 14:37:21', '2019-06-10 14:37:21'),
(60, '', NULL, 'syr@gmailcmo', '987987989', '2', '', NULL, '0', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjpudWxsfQ.9lvpmeQZHsbFGYA7uW685SVqcqFTiGUoD4jzqMT5Aok', '2019-06-10 15:08:02', '2019-06-10 15:08:02'),
(63, '', NULL, 'suryaknts@asd.com', '147852326565', '2', '', NULL, '0', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjpudWxsfQ.9lvpmeQZHsbFGYA7uW685SVqcqFTiGUoD4jzqMT5Aok', '2019-06-10 15:29:36', '2019-06-10 15:29:36'),
(64, '', NULL, 'suryaknt123s@asd.com', '12312312', '2', '', NULL, '0', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjoic3VyeWFrbnQxMjNzQGFzZC5jb20ifQ.B2L_9T5gc3LuPocKJLaGx5ftnzezVCpw2NLAXhn45Ms', '2019-06-10 16:41:02', '2019-06-10 16:41:02'),
(65, '', NULL, 'patil@asd.com', '234234', '2', '', NULL, '0', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjoicGF0aWxAYXNkLmNvbSJ9.9-BrYuL6M0hlLOuI7ul-ugSH8g0OTOWCsX5nkV2AiWo', '2019-06-10 16:43:19', '2019-06-10 16:43:19'),
(66, '', NULL, 'surya@gmail.com', '234234234', '2', '', NULL, '0', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjoic3VyeWFAZ21haWwuY29tIn0.OGozpV3CYr3r6741uH03rnQ8TtOT9IxMgKVj2gXz6Gg', '2019-06-10 16:45:54', '2019-06-10 16:45:54'),
(67, '', NULL, 'eeeee12@asd.com', '147852326565', '2', '', NULL, '0', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjoiZWVlZWUxMkBhc2QuY29tIn0.MC2v74TmKVjWfN_depQxGv1kuv2AsTo61oUqwnqAwSE', '2019-06-10 16:47:42', '2019-06-10 16:47:42'),
(68, '', NULL, 'cZZ@asdsa.com', '545478867', '2', '', NULL, '0', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjoiY1paQGFzZHNhLmNvbSJ9.sW5ctjiiGgfx_BvZZzlsTqfAWh2BDctRLffjoq8OmDY', '2019-06-10 16:51:02', '2019-06-10 16:51:02'),
(70, '', NULL, 'cZZ@asdsa.comd', '5454788673', '2', '', NULL, '0', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjoiY1paQGFzZHNhLmNvbWQifQ.O3FHmlFyHOxY5QfV6zjo7nGefUTQfS51Pb3ZWLPlDWA', '2019-06-10 16:56:00', '2019-06-10 16:56:00'),
(71, '', NULL, 'cZZ@asdsa.comdwe', '545478867323', '2', '', NULL, '0', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjoiY1paQGFzZHNhLmNvbWR3ZSJ9.RVky8T-4w_V5xTRcKRpFhZr1RIo87NcR3phpohJhJ_8', '2019-06-10 16:58:14', '2019-06-10 16:58:14'),
(72, '', NULL, 'cZZ@asdsa.comdwe11', '54547886732311', '2', '', NULL, '0', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjoiY1paQGFzZHNhLmNvbWR3ZTExIn0.z86OtMFakkHlJMjN5N9LO-m-wtjoPcg4wvNBXvfLgnU', '2019-06-10 17:02:56', '2019-06-10 17:02:56'),
(73, '', NULL, 'cZZ@asdsa.cssomdwe11', '545478867322311', '2', '', NULL, '0', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjoiY1paQGFzZHNhLmNzc29tZHdlMTEifQ.Hej5MTn5GlwNWehsEgnIEhMugIiSCmCsH0oFlkUQQUg', '2019-06-10 17:05:35', '2019-06-10 17:05:35'),
(74, '', NULL, 'cZZ@asdsa.cssomdwe11s', '54547886732231122', '2', '', NULL, '0', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjoiY1paQGFzZHNhLmNzc29tZHdlMTFzIn0.4iOvCn8J_bRc0qqGCiQThABZwtBqsSU_xBLJc0wsMjE', '2019-06-10 17:06:24', '2019-06-10 17:06:24'),
(75, '', NULL, 'cZZ@asdsa.cssou', '545478867322311228', '2', '', NULL, '0', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjoiY1paQGFzZHNhLmNzc291In0.8eRLERATB9NTVF2bQEzDXbeczgmLBkiO0M-jLsAkq-4', '2019-06-10 17:19:28', '2019-06-10 17:19:28'),
(76, '', NULL, 'cZZ@asdsa.cssouqq', '545478864', '2', '', NULL, '0', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjoiY1paQGFzZHNhLmNzc291cXEifQ.DV4q8YGLh1ZNQfkGYKdnkE_T7tWPdHamwlvh5GNhg90', '2019-06-10 17:20:15', '2019-06-10 17:20:15'),
(77, '', 'patil', 'eeee1234e@asd.com', '147852326565', '2', '', NULL, '0', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjoiZWVlZTEyMzRlQGFzZC5jb20ifQ.3OVUDfCOQi20g3VeI5-fnCdwurQghGbNpOWVDqh3oxY', '2019-06-10 17:23:22', '2019-06-10 17:23:22'),
(78, 'suryakant', 'yengudle', 'suryaknts2134@asd.com', '1242314123', '2', 'e10adc3949ba59abbe56e057f20f883e', NULL, '1', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjoic3VyeWFrbnRzMjEzNEBhc2QuY29tIn0.4MPlp9xtCFrW9g5c321J1_LUNJNmUBqFqyyPBMOi0lY', '2019-06-12 07:46:42', '2019-06-10 17:25:49'),
(79, 'sadsadsff', 'sadsads', 'cZZ@asdsa.cssouqqoo', '54547886400', '2', '', NULL, '0', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjoiY1paQGFzZHNhLmNzc291cXFvbyJ9.rjTbO5UBrUZi2YWl3iOnM_03lxFS15EEhY2GqhBigNc', '2019-06-10 17:28:45', '2019-06-10 17:28:45'),
(80, 'sdf', 'mumer', '', '989879', '4', '', NULL, '0', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjoiIn0.AkBMVagpvBQwB9SHx1cym4Yead-opAPKKR6V6Ac63J0', '2019-06-11 14:54:59', '2019-06-11 14:54:59'),
(81, 'surys', 'kljsdkf', 'kumer12@gmil.com', '9875642310', '4', '', NULL, '0', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjoia3VtZXIxMkBnbWlsLmNvbSJ9.BKnsOz1ZpjgdhIvAs_C9i7RXdRnRjleB0rUhBi5OvDc', '2019-06-11 15:02:04', '2019-06-11 15:02:04'),
(82, 'jklasdjf', 'jasjkdfsj', 'father@gmailc.om', '789798798798', '4', '', NULL, '0', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjoiZmF0aGVyQGdtYWlsYy5vbSJ9.Iz2XswkvR-OXaItCuQqZtL0FMoug7KVQKL4nUmpliqY', '2019-06-12 13:15:28', '2019-06-12 13:15:28'),
(83, 'suryagmail.com', 'patil', 'surya1234432@gmail.com', '79884561313', '4', '', NULL, '0', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjoic3VyeWExMjM0NDMyQGdtYWlsLmNvbSJ9.qTJnUWwd8Ki4_0MDSlaWabUOeWw31awWhyPhyllhUlo', '2019-06-12 14:33:21', '2019-06-12 14:33:21'),
(84, 'Harshal', 'patil', 'patil@aklsdjf.com', '9877989789', '2', '', NULL, '0', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjoicGF0aWxAYWtsc2RqZi5jb20ifQ.9BksqROrEWW7SlosAI0Q364cS2cb4EAyLZyonuPWNGw', '2019-06-12 20:20:36', '2019-06-12 20:20:36'),
(85, 'parent 8s', 'patil', 'eeeeasdase@asd.com', '147852326565', '2', '', NULL, '0', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjoiZWVlZWFzZGFzZUBhc2QuY29tIn0.QpcxeSHPhZ8sWbgEfnFObLCzBB9XKf0QH62cU-KHs7s', '2019-06-12 20:39:57', '2019-06-12 20:39:57'),
(86, 'snehaqwe', 'patil', 'eeeewqee@asd.com', '147852326565', '2', '', NULL, '0', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjoiZWVlZXdxZWVAYXNkLmNvbSJ9.9CIYgu7gM8kCu_T9lTrmMzlqSlu5GoXgr0U9dcE2HVE', '2019-06-12 20:50:34', '2019-06-12 20:50:34'),
(87, 'anand', 'patil', 'patilan@gmail.comn', '5648972310', '2', '', NULL, '0', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjoicGF0aWxhbkBnbWFpbC5jb21uIn0.Ogc_vCwYUFzk8sG6j44as13Mo_C4OIwCyebv6NqnSfM', '2019-06-13 12:33:36', '2019-06-13 12:33:36'),
(88, 'suryakantasd', '2134', 'suryaknt123s@asd.com', '147852326565', '2', '', NULL, '0', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjoic3VyeWFrbnQxMjNzQGFzZC5jb20ifQ.B2L_9T5gc3LuPocKJLaGx5ftnzezVCpw2NLAXhn45Ms', '2019-06-13 12:35:01', '2019-06-13 12:35:01'),
(89, 'parent 8s', 'patil', 'suryaknt1ds2.asds@asd.com', '147852326565', '2', '', NULL, '0', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjoic3VyeWFrbnQxZHMyLmFzZHNAYXNkLmNvbSJ9.DP1As1fdAiqPOo_vJ7q4pXYYypLe_Dw-V4rYO171CFo', '2019-06-14 17:32:49', '2019-06-14 17:32:49');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `consultation`
--
ALTER TABLE `consultation`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `Student_id` (`Student_id`),
  ADD KEY `Doctor_id` (`Doctor_id`);

--
-- Indexes for table `doctor`
--
ALTER TABLE `doctor`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `User_id` (`User_id`);

--
-- Indexes for table `eye_check`
--
ALTER TABLE `eye_check`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `master_column`
--
ALTER TABLE `master_column`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `parent_post_data`
--
ALTER TABLE `parent_post_data`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `school`
--
ALTER TABLE `school`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `school_ibfk_1` (`User_id`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `Mother_mobile` (`Father_mobile`,`Mother_email`),
  ADD UNIQUE KEY `Mother_mobile_2` (`Mother_mobile`),
  ADD KEY `User_id` (`User_id`);

--
-- Indexes for table `student_health`
--
ALTER TABLE `student_health`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `Student_id` (`Student_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `Email` (`Email`,`Mobile`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `consultation`
--
ALTER TABLE `consultation`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `doctor`
--
ALTER TABLE `doctor`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `eye_check`
--
ALTER TABLE `eye_check`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `master_column`
--
ALTER TABLE `master_column`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `parent_post_data`
--
ALTER TABLE `parent_post_data`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT for table `school`
--
ALTER TABLE `school`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `student_health`
--
ALTER TABLE `student_health`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=90;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `doctor`
--
ALTER TABLE `doctor`
  ADD CONSTRAINT `doctor_ibfk_1` FOREIGN KEY (`User_id`) REFERENCES `users` (`Id`);

--
-- Constraints for table `school`
--
ALTER TABLE `school`
  ADD CONSTRAINT `school_ibfk_1` FOREIGN KEY (`User_id`) REFERENCES `users` (`Id`);

--
-- Constraints for table `student`
--
ALTER TABLE `student`
  ADD CONSTRAINT `student_ibfk_1` FOREIGN KEY (`User_id`) REFERENCES `users` (`Id`);

--
-- Constraints for table `student_health`
--
ALTER TABLE `student_health`
  ADD CONSTRAINT `student_health_ibfk_1` FOREIGN KEY (`Student_id`) REFERENCES `student` (`Id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
